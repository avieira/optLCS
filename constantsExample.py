import numpy as np
from casadi import SX

#Uncomment these lines until x0 for a 2D example (and comment the 1D example)
#A=SX([[1,2],[2,1]])
#B=SX([[-1,1],[-1,1]])
#C=SX([[3,-1],[-2,0]])
#D=SX.eye(2)
#E=SX([[1,-1],[-1,2]])
#F=SX([[1,3],[2,1]])
#
#Q=SX.eye(2)
#U=25*SX.eye(2)
#
#x0=np.array([-0.5,1])

#Uncomment these lines until x0 for a 1D example
A=SX(3)
B=SX(-0.5)
C=SX(0)
D=SX.eye(1)
E=SX(-2)
F=SX(3)

Q=SX.eye(1)
U=SX.eye(1)

x0=np.array([1])


theta=0.5

T=1.0
nbStepMethDirecte=100
nbStepMethIndirecte=1000
nbIntervShooting=50 #must devide nbStepMethIndirecte

#Used for dual method
r=1e-2
