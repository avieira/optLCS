# optLCS
This code is used to numerically solve the Optimal Control Problem for Linear Complementarity Systems:

<p align="center"><img alt="$$&#10;&#9;\min\ \int_0^T (x(s)^\intercal Qx(s) + u(s)^\intercal Uu(s))ds&#10;$$" src="svgs/f2ee64434606e5fc42bb17be179f0fab.png?invert_in_darkmode" align=middle width="272.97105pt" height="41.121795pt"/></p>
<p align="center"><img alt="$$&#9;\text{s.t. } \dot{x}(t)=Ax(t)+Bv(t)+Eu(t) \text{ a.e. on } [0,T]$$" src="svgs/60572b2abd9f5fc1ca6ffa5101a3277b.png?invert_in_darkmode" align=middle width="337.80614999999995pt" height="16.376943pt"/></p>
<p align="center"><img alt="$$ &#9;\hspace{4em} 0\leq v(t) \perp Cx(t)+Dv(t)+Eu(t)\geq 0 \text{ a.e. on } [0,T] $$" src="svgs/0a9ef5c104ed0d32c30c6f9e59862d44.png?invert_in_darkmode" align=middle width="369.10664999999995pt" height="16.376943pt"/></p>

## Installation
This code needs [CasADI](https://github.com/casadi/casadi) 3.x to run. Make sure to add it to your PYPATH!

## Tests
Run `example.py [direct|indirect]` to see an example used for paper (soon ;) ). You can choose between two examples (check `constantsExample.py`), and between direct and indirect when launching the program.
Of course, the code is designed to work with any dimension. Use it as you want!
