#!/usr/bin/python3
import sys
sys.path.append("./casadi")

from casadi import SX, vertcat

from lcs.optimal import OptLCSDirect,OptLCSIndirect

font={'weight':'bold',
     'size':14}


def launchDirect():
    from constantsExample import A, B, C, D, E, F, Q, U, T, x0, theta, nbStepMethDirecte
    pblmOptDirect=OptLCSDirect(A, B, C, D, E, F, Q, U, 0, T)
    pblmOptDirect.nbStepDirect=nbStepMethDirecte
    pblmOptDirect.x0=x0

    resultD=pblmOptDirect.compute_optimal(method='relax',dual=True,verbose=0, print_level_ipopt=0)

    pblmOptDirect.resultVis(resultD, dual=True, font=font , save=False)

def launchIndirect():
    from constantsExample import A, B, C, D, E, F, Q, U, T, x0, theta, nbStepMethDirecte, nbStepMethIndirecte, nbIntervShooting, r
    #exec(open("./constants-example.py").read())
    pblmOptIndirect=OptLCSIndirect(A, B, C, D, E, F, Q, U, 0, T, r)
    pblmOptIndirect.setNbStep(nbStepMethDirecte, nbStepMethIndirecte, nbIntervShooting)
    pblmOptIndirect.x0=x0

    M=SX(2*pblmOptIndirect.n,2*pblmOptIndirect.n)
    M[0:pblmOptIndirect.n,0:pblmOptIndirect.n]=SX.eye(pblmOptIndirect.n)

    R=SX(2*pblmOptIndirect.n,2*pblmOptIndirect.n)
    R[pblmOptIndirect.n:2*pblmOptIndirect.n,pblmOptIndirect.n:2*pblmOptIndirect.n]=SX.eye(pblmOptIndirect.n)

    b=SX.zeros(2*pblmOptIndirect.n)
    b[0:pblmOptIndirect.n]=SX(vertcat(*x0))

    resultInd=pblmOptIndirect.compute_optimal(M,R,b, print_level_ipopt=0, verbose=0, maxLoops=15)

    pblmOptIndirect.resultVis(resultInd, dual=True, font=font, save=False)


def main(argv):
    try:
        if argv==[]:
            raise ValueError
        if not argv[0] in ['direct', 'indirect']:
            raise ValueError
    except ValueError:
        print('         example.py [direct|indirect]')
        print('         Constants used for this example should be changed in constantsExample.py')
        sys.exit(2)
    if argv[0]=='direct':
        launchDirect()
    else:
        launchIndirect()



if __name__ == "__main__":
    main(sys.argv[1:])
