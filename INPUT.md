# optLCS
This code is used to numerically solve the Optimal Control Problem for Linear Complementarity Systems:

$$
	\min\ \int_0^T (x(s)^\intercal Qx(s) + u(s)^\intercal Uu(s))ds
$$
$$	\text{s.t. } \dot{x}(t)=Ax(t)+Bv(t)+Eu(t) \text{ a.e. on } [0,T]$$
$$ 	\hspace{4em} 0\leq v(t) \perp Cx(t)+Dv(t)+Eu(t)\geq 0 \text{ a.e. on } [0,T] $$

## Installation
This code needs [CasADI](https://github.com/casadi/casadi) 3.x to run. Make sure to add it to your PYPATH!

## Tests
Run `example.py [direct|indirect]` to see an example used for paper (soon ;) ). You can choose between two examples (check `constantsExample.py`), and bewteen direct and indirect when launching the program.
Of course, the code is designed to work with any dimension. Use it as you want!
