from casadi import DM, SX, nlpsol, vertcat, mtimes, inv, transpose, solve, Function, if_else, reshape
from numpy import inf, floor
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os

from lcs.LCS import LCS
from lcs.optimal.OptLCSDirect import OptLCSDirect
"""
    General class for optimal control problem of the type:
    .. math::
   :nowrap:

       \begin{eqnarray}
          \int_{t0}^{tf} (x(t)^\intercal Qx(t)+u(t)^\intercal Uu(t)) dt
          \dot{x}=Ax+Bv+Fu
          0\leq v \perp Cx+Dv+Fu \geq 0
          Mx(0) + Nx(T)= b
       \end{eqnarray}

    using indirect method.

    :param x0: state at time t0 if fixed
    :type x0: np.ndarray

    :param xf: state at time tf if fixed
    :type xf: np.ndarray
    
    .. seealso:: 
"""
class OptLCSIndirect(OptLCSDirect):
   
    def __init__(self, A, B, C, D, E, F, Q, U, t0, tf, pertComp, theta=0.5):
        OptLCSDirect.__init__(self, A, B, C, D, E, F, Q, U, t0, tf)
        self.r=pertComp
        self.theta=theta

    def setNbStep(self,nbStepDirect,nbStepIndirect,nbIntervShooting):
        self.nbStepIndirect=nbStepIndirect
        self.nbStepDirect=nbStepDirect
        self.hDirect=(self.tf-self.t0)/float(nbStepDirect)
        self.hIndirect=(self.tf-self.t0)/float(nbStepIndirect)
        
        self.nbIntervShooting=nbIntervShooting
        self.nbStepInterv=int(nbStepIndirect/nbIntervShooting)
        
    
    """
        Compute a numerical approximation of:
        .. math::
       :nowrap:

           \begin{eqnarray}
              \begin{pmatrix} \dot{x} \\ \dot{p} \end{pmatrix}    & = & A \begin{pmatrix} x \\ p \end{pmatrix} + B \begin{pmatrix} \beta \\ v \end{pmatrix}  \\
              0\leq \begin{pmatrix} \beta \\ v \end{pmatrix} \perp C \begin{pmatrix} x \\ p \end{pmatrix} + D \begin{pmatrix} \beta \\ v \end{pmatrix} \geq 0
              M \begin{pmatrix} x(0) \\ p(0) \end{pmatrix} + N \begin{pmatrix} x(T) \\ p(T) \end{pmatrix} = b
           \end{eqnarray}

        using multiple shooting.

        :param initPoints: dictionnary containing two fields, "x" and "p", which are 2D DM arrays of size (nbInterv+1,dimState) containing the initial values for x and p at times of shooting.
        :type initPoints: dict

        :param guessSol: dictionnary containing four fields, "x", "p", "beta" and "v", which are 2D DM arrays of size (nbPtsDiscr+1,dimState) for x and p, (nbPtsDiscr+1,dimCtrl) and beta and v. It contains an approximate guess of the solution trajectory.
        :type guessSol: dict

        :param nbInterv: number of sub-intervals for multiple shooting
        :type nbInterv: integer

        :param nbPtsDiscr: number of discretization points on the whole interval
        :type nbPtsDiscr: integer

        :param nbPtsPerInterv: number of discretization points on each sub-interval
        :type nbPtsPerInterv: integer
        ..todo: Change this to have different number of points on each sub interval

        :param theta: parameter used for the theta method
        :type theta: float


        :return: dict containing a computed value of:
            - 'x'
            - 'p'
            - 'v'
            - 'beta'
            - 'gap' : the gap between the computed value via integration at the end of a sub-interval and initial point at the beginning of the next one
            - 'jacob' : the sensitivity matrix of the value at each end of sub-interval accroding to the value at the beggining of the sub-interval
        The two last arguments are used for a Newton method in order to solve the BVP.
        :rtype: dict

        .. seealso:: newtonSolveShooting


        .. warning:: Imports to do beforehand :
        import numpy as np
        from casadi import DM, SX, reshape, nlpsol, vertcat, mtimes, inv, transpose
    """
    def integDVIShooting(self, initPoints, guessSol, A, B, C, D, M, N, b, theta, printlvl):

        betaNumDual=DM.zeros((self.nbStepIndirect+1),self.m)
        vNumDual=DM.zeros((self.nbStepIndirect+1),self.m)
        xNumDual=DM.zeros((self.nbStepIndirect+1),self.n)
        pNumDual=DM.zeros((self.nbStepIndirect+1),self.n)
        ecartNewton=DM.zeros(2*self.n*(self.nbIntervShooting+1))
        sensitJacob=SX(2*self.n*(self.nbIntervShooting+1),2*self.n*(self.nbIntervShooting+1))

        for interv in range(self.nbIntervShooting):
            lxb=[]
            uxb=[]

            betaInterv=[]
            for i in range(self.nbStepInterv+1):
                betaInterv.append(SX.sym('betaInterv'+str(i),self.m))
                lxb.append(-inf*np.ones(self.m))
                uxb.append(inf*np.ones(self.m))

            vInterv=[]
            for i in range(self.nbStepInterv+1):
                vInterv.append(SX.sym('vInterv'+str(i),self.m))
                lxb.append(-inf*np.ones(self.m))
                uxb.append(inf*np.ones(self.m))

            betavInterv=[]
            for i in range(self.nbStepInterv+1):
                betavInterv.append(vertcat(*[betaInterv[i],vInterv[i]]))

            xInterv=[]
            #Initial Condition
            xInterv.append(SX.sym('xInterv_0',self.n))
            lxb.append(vertcat(*(initPoints["x"][interv,:]).full()))
            uxb.append(vertcat(*(initPoints["x"][interv,:]).full()))
            #Middle points
            for i in range(1,self.nbStepInterv+1):
                xInterv.append(SX.sym('xInterv'+str(i),self.n))
                lxb.append(-inf*np.ones(self.n))
                uxb.append(inf*np.ones(self.n))

            pInterv=[]
            #Initial Condition
            pInterv.append(SX.sym('pInterv_0',self.n))
            lxb.append(vertcat(*(initPoints["p"][interv,:]).full()))
            uxb.append(vertcat(*(initPoints["p"][interv,:]).full()))
            #Middle points
            for i in range(1,self.nbStepInterv+1):
                pInterv.append(SX.sym('pInterv'+str(i),self.n))
                lxb.append(-inf*np.ones(self.n))
                uxb.append(inf*np.ones(self.n))

            zInterv=[]
            for i in range(self.nbStepInterv+1):
                zInterv.append(vertcat(*[xInterv[i],pInterv[i]]))

            obj=0
            for index in range(self.nbStepInterv+1):
                obj+=mtimes(transpose(betavInterv[index]),mtimes(D,betavInterv[index])\
                            +mtimes(C,zInterv[index]))

            g=[]
            lgb=[]
            ugb=[]

            for i in range(self.nbStepInterv+1):
                g.append(mtimes(D,betavInterv[i])+mtimes(C,zInterv[i]))
                lgb.append(np.zeros(2*self.m))
                ugb.append(inf*np.ones(2*self.m))

            for i in range(self.nbStepInterv+1):
                g.append(mtimes(self.matg,betavInterv[i]))
                lgb.append(np.zeros(2*self.m))
                ugb.append(inf*np.ones(2*self.m))

            for i in range(self.nbStepInterv):
                g.append((zInterv[i+1]-zInterv[i])/self.hIndirect\
                         -theta*(mtimes(A,zInterv[i+1])+mtimes(B,betavInterv[i+1]))\
                         -(1-theta)*(mtimes(A,zInterv[i])+mtimes(B,betavInterv[i]))\
                        )
                lgb.append(np.zeros(2*self.n))
                ugb.append(np.zeros(2*self.n))

            nlp={'x':vertcat(*[vertcat(*betaInterv),vertcat(*vInterv),vertcat(*xInterv),vertcat(*pInterv)]),\
                 'f':obj,\
                 'g':vertcat(*g)}
            solver=nlpsol('solver','ipopt',nlp,
                            {'ipopt':{'print_level':printlvl,\
                             'fixed_variable_treatment':'make_constraint',\
                             "nlp_scaling_method":"none",\
                             'constr_viol_tol':1e-10}})

            v0=[]
            v0.append(reshape(guessSol["beta"][interv*(self.nbStepInterv):(interv+1)*(self.nbStepInterv)+1,:].T,\
                              self.m*(self.nbStepInterv+1),1))
            v0.append(reshape(guessSol["v"][interv*(self.nbStepInterv):(interv+1)*(self.nbStepInterv)+1,:].T,\
                              self.m*(self.nbStepInterv+1),1))
            v0.append(reshape(guessSol["x"][interv*(self.nbStepInterv):(interv+1)*(self.nbStepInterv)+1,:].T,\
                              self.n*(self.nbStepInterv+1),1))
            v0.append(reshape(guessSol["p"][interv*(self.nbStepInterv):(interv+1)*(self.nbStepInterv)+1,:].T,\
                              self.n*(self.nbStepInterv+1),1))
                      
            v0=vertcat(*v0)

            sol = solver(x0=v0,
                         lbx=vertcat(*lxb), 
                         ubx=vertcat(*uxb),
                         lbg=vertcat(*lgb), 
                         ubg=vertcat(*ugb))

            solNum=sol['x']    
            betaNumDual[self.nbStepInterv*interv:self.nbStepInterv*(interv+1),:]=\
                    DM(reshape(solNum[0:self.m*self.nbStepInterv],self.m,self.nbStepInterv).T)
            vNumDual[self.nbStepInterv*interv:self.nbStepInterv*(interv+1),:]=\
                    DM(reshape(solNum[self.m*(self.nbStepInterv+1):2*self.m*(self.nbStepInterv+1)-self.m]\
                               ,self.m,self.nbStepInterv).T)
            xNumDual[self.nbStepInterv*interv:self.nbStepInterv*(interv+1),:]=\
                    DM(reshape(solNum[2*self.m*(self.nbStepInterv+1):(2*self.m+self.n)*(self.nbStepInterv+1)-self.n],\
                               self.n,self.nbStepInterv).T)
            pNumDual[self.nbStepInterv*interv:self.nbStepInterv*(interv+1),:]=\
                    DM(reshape(solNum[(2*self.m+self.n)*(self.nbStepInterv+1):2*(self.m+self.n)*(self.nbStepInterv+1)-self.n]\
                               ,self.n,self.nbStepInterv).T)

            xFinal=solNum[(2*self.m+self.n)*(self.nbStepInterv+1)-self.n:(2*self.m+self.n)*(self.nbStepInterv+1)]
            pFinal=solNum[2*(self.m+self.n)*(self.nbStepInterv+1)-self.n:2*(self.m+self.n)*(self.nbStepInterv+1)]
            ecartNewton[2*self.n*interv:2*self.n*(interv+1)]=DM(vertcat(*[initPoints["x"][interv+1,:].T-xFinal,\
                                                                initPoints["p"][interv+1,:].T-pFinal]))

            if interv==self.nbIntervShooting-1:
                xNumDual[-1,:]=xFinal
                pNumDual[-1,:]=pFinal
                ecartNewton[2*self.n*(interv+1):2*self.n*(interv+2)]=DM(b\
                                    -mtimes(M,vertcat(*[initPoints["x"][0,:].T,initPoints["p"][0,:].T]))\
                                    -mtimes(N,vertcat(*[xFinal,pFinal])))

                betaNumDual[-1,:]=solNum[self.m*self.nbStepInterv:self.m*(self.nbStepInterv+1)]
                vNumDual[-1,:]=solNum[2*self.m*(self.nbStepInterv+1)-self.m:2*self.m*(self.nbStepInterv+1)]

            sensitJacobInterv=DM.eye(2*self.n)
            for i in range(1,self.nbStepInterv):
                wMult=mtimes(D,vertcat(*[betaNumDual[interv*self.nbStepInterv+i,:].T,vNumDual[interv*self.nbStepInterv+i,:].T]))\
                    +mtimes(C,vertcat(*[xNumDual[interv*self.nbStepInterv+i,:].T,pNumDual[interv*self.nbStepInterv+i,:].T]))

                setJ=[]
                for j in range(2*self.m):
                    if wMult[j]<2*self.hIndirect:
                        setJ.append(j)

                if setJ:
                    invDTD=inv(mtimes(transpose(D[setJ,setJ]),D[setJ,setJ]))
                    DTC=mtimes(transpose(D[setJ,setJ]),C[setJ,:])
                    Th=-mtimes(B[:,setJ],mtimes(invDTD,DTC))
                else:
                    Th=DM.zeros(2*self.n,2*self.n)

                sensitJacobInterv=mtimes(DM.eye(2*self.n)+self.hIndirect*(A+Th),sensitJacobInterv)

            sensitJacob[2*self.n*interv:2*self.n*(interv+1),2*self.n*interv:2*self.n*(interv+1)]=sensitJacobInterv
            sensitJacob[2*self.n*interv:2*self.n*(interv+1),2*self.n*(interv+1):2*self.n*(interv+2)]=-DM.eye(2*self.n)
        
        sensitJacob[2*self.n*self.nbIntervShooting:2*self.n*(self.nbIntervShooting+1),0:2*self.n]=M
        sensitJacob[2*self.n*self.nbIntervShooting:2*self.n*(self.nbIntervShooting+1),\
                    2*self.n*self.nbIntervShooting:2*self.n*(self.nbIntervShooting+1)]=N

        return {'x':xNumDual, 'p':pNumDual, 'v':vNumDual, 'beta':betaNumDual, 'gap':ecartNewton, 'jacob':sensitJacob}
    
    """
        Compute the next iteration for a Newton method to solve a BVP, in a shooting framework.

        :param jacobian: sensitivity matrix of the value at each end of sub-interval accroding to the value at the beggining of the sub-interval
        :type jacobian: 2D SX array

        :param gap: gap between the computed value via integration at the end of a sub-interval and initial point at the beginning of the next one
        :type gap: 1D DM array

        :param guessPrev: initial points at each sub-interval at previous iteration
        :type guessPrev: array

        :param nbInterv: number of sub-intervals for multiple shooting
        :type nbInterv: integer

        :param dimState: dimension for x and p
        :type dimState: integer

        :return: dictionnary containing two fields, "x" and "p", containing initial values at each sub-interval to be used for integration of the BVP.
        :rtype: dict

        .. seealso:: integDVIShooting

        .. warning:: Imports to do beforehand :
        from casadi import DM, SX, solve, vertcat
    """

    def newtonSolveShooting(self, jacobian,gap,guessPrev):
        initGuessNext=DM(np.linalg.solve(DM(jacobian).full(),gap.full()))+vertcat(*guessPrev)
        xInitGuess=DM(self.nbIntervShooting+1,self.n)
        pInitGuess=DM(self.nbIntervShooting+1,self.n)
        for i in range(self.nbIntervShooting+1):
            xInitGuess[i,:]=DM(initGuessNext[2*self.n*i:2*self.n*i+self.n])
            pInitGuess[i,:]=DM(initGuessNext[2*self.n*i+self.n:2*self.n*(i+1)])

        return {"x":xInitGuess,"p":pInitGuess}

    def buildMatricesIndirectSystem(self, zeta):
        invU=inv(self.U)
        FinvU=mtimes(self.F,invU)
        EinvU=mtimes(self.E,invU)

        compMultMatdm=DM(2*self.m,2*self.m)
        compMultMatdm[0:self.m,0:self.m]=DM(mtimes(EinvU,transpose(self.E)))
        compMultMatdm[0:self.m,self.m:2*self.m]=DM(self.D-(zeta+self.r)*mtimes(EinvU,transpose(self.E)))
        compMultMatdm[self.m:2*self.m,0:self.m]=DM(zeta*mtimes(EinvU,transpose(self.E))-transpose(self.D))
        compMultMatdm[self.m:2*self.m,self.m:2*self.m]=DM(zeta*self.D\
                                                          +(zeta+self.r)*(transpose(self.D)\
                                                                               -zeta*mtimes(EinvU,transpose(self.E))))

        compStateMatdm=DM(2*self.m,2*self.n)
        compStateMatdm[0:self.m,0:self.n]=DM(self.C)
        compStateMatdm[0:self.m,self.n:2*self.n]=DM(mtimes(EinvU,transpose(self.F)))
        compStateMatdm[self.m:2*self.m,0:self.n]=DM(zeta*self.C)
        compStateMatdm[self.m:2*self.m,self.n:2*self.n]=DM(zeta*mtimes(EinvU,transpose(self.F))-transpose(self.B))

        dynStateMatdm=DM(2*self.n,2*self.n)
        dynStateMatdm[0:self.n,0:self.n]=DM(self.A)
        dynStateMatdm[0:self.n,self.n:2*self.n]=DM(mtimes(FinvU,transpose(self.F)))
        dynStateMatdm[self.n:2*self.n,0:self.n]=DM(self.Q)
        dynStateMatdm[self.n:2*self.n,self.n:2*self.n]=DM(-transpose(self.A))

        dynMultMatdm=DM(2*self.n,2*self.m)
        dynMultMatdm[0:self.n,0:self.m]=DM(mtimes(FinvU,transpose(self.E)))
        dynMultMatdm[0:self.n,self.m:2*self.m]=DM(self.B-(zeta+self.r)*mtimes(FinvU,transpose(self.E)))
        dynMultMatdm[self.n:2*self.n,0:self.m]=DM(-transpose(self.C))
        dynMultMatdm[self.n:2*self.n,self.m:2*self.m]=DM((zeta+self.r)*transpose(self.C))

        invImhA=inv(DM.eye(2*self.n)-self.hIndirect*dynStateMatdm)
        matDh=compMultMatdm+self.hIndirect*mtimes(compStateMatdm,mtimes(invImhA,dynMultMatdm))
        matEh=mtimes(compStateMatdm,invImhA)

        matg=DM(2*self.m,2*self.m)
        matg[0:self.m,0:self.m]=DM.eye(self.m)
        matg[0:self.m,self.m:2*self.m]=-self.r*DM.eye(self.m)
        matg[self.m:2*self.m,self.m:2*self.m]=DM.eye(self.m)
        
        return (dynStateMatdm, dynMultMatdm, compStateMatdm, compMultMatdm, invU, matg)
        
    def buildFirstGuess(self, resultDirect):
        xNum=resultDirect['x']
        pNum=resultDirect['p']
        vNum=resultDirect['v']
        betaNum=resultDirect['beta']
        N=self.nbStepIndirect
        
        xNumDual=DM.zeros(N+1,self.n)
        pNumDual=DM.zeros(N+1,self.n)
        vNumDual=DM.zeros(N+1,self.m)
        betaNumDual=DM.zeros(N+1,self.m)

        stepPerDirInterv=int(floor(self.nbStepIndirect/self.nbStepDirect))
        for dirIndex in range(self.nbStepDirect-1):
            for i in range(stepPerDirInterv):
                lambd=(1.0*i)/(stepPerDirInterv)

                xNumDual[dirIndex*stepPerDirInterv+i,:]=((1-lambd)*xNum[dirIndex,:]+lambd*xNum[dirIndex+1,:])
                pNumDual[dirIndex*stepPerDirInterv+i,:]=(1-lambd)*pNum[dirIndex,:]+lambd*pNum[dirIndex+1,:]
                vNumDual[dirIndex*stepPerDirInterv+i,:]=vNum[dirIndex,:]
                betaNumDual[dirIndex*stepPerDirInterv+i,:]=betaNum[dirIndex,:]
                

        for i in range(N+1-stepPerDirInterv-1,N+1):
            xNumDual[i,:]=xNum[-1,:]
            pNumDual[i,:]=pNum[-1,:]
            vNumDual[i,:]=vNum[-1,:]
            betaNumDual[i,:]=betaNum[-1,:]

        initGuessTotal={"x":xNumDual,"p":pNumDual,"v":vNumDual,"beta":betaNumDual}

        xShooting=DM(self.nbIntervShooting+1,self.n)
        pShooting=DM(self.nbIntervShooting+1,self.n)
        for index in range(self.nbIntervShooting+1):
            xShooting[index,:]=xNumDual[index*self.nbStepInterv,:]
            pShooting[index,:]=pNumDual[index*self.nbStepInterv,:]
        initGuessShooting={"x":xShooting,"p":pShooting}
        
        return (initGuessTotal, initGuessShooting)
    

    """
        Compute an approximate optimal solution using indirect method. More exactely it searches for a S-stationary solution complying with border condition:
       .. math::
       :nowrap:

           \begin{eqnarray}
              M \begin{pmatrix} x(0) \\ p(0) \end{pmatrix} + R \begin{pmatrix} x(T) \\ p(T) \end{pmatrix} = b
           \end{eqnarray}
        where p is the adjoint state.
        
        In order to deal with the complementarity, 2 methods are available, taken for the litterature : augmentation of the cost, or relaxation of the complementarity.

        :param theta: theta for theta scheme.
        :type theta: float

        :param methodDirect: define what method should be used for computing an initial guess of the optimal solution using Direct Method. "relax" for relaxation of the complementarity, "augment" for augmentation of the cost.
        :type methodDirect: string

        :param maxLoops: maximum iterations for Newton method allowed
        :type maxLoops: integer
        
        :param print_level_ipopt: Level of information given by ipopt 
        :type print_level_ipopt: integer between 0 and 9
        
        :param verbose: Control verbosity
        :type verbose: boolean
        
        :param epsLam: value under which v and w are considered as 0 (used to compute zeta from the initial guess)
        :type epsLam: float
        
        :return: dict containing:
            - optimal "x", "u" and "v", and associated "w"
            - adjoint state "p"
            - multipliers "lamG" and "lamH"
            - 'zeta' used for defining the S-stationarity system
            - 'listGap' : maximum gaps at shooting nodes for (x,p), used to stop Newton iterations
        
        .. todo:: Check how to better manage border conditions concerning p
        .. seealso:: OptLCSDirect
    """
    def compute_optimal(self, M, R, b, theta=0.5, methodDirect="relax", maxLoops=10, print_level_ipopt=0, verbose=0, epsLam=1e-5):
        
        resultDirect=OptLCSDirect.compute_optimal(self,method=methodDirect,\
                                     dual=True, print_level_ipopt=print_level_ipopt,\
                                     verbose=verbose)
        
        listZeta=[0]
        for t in range(self.nbStepDirect-1):
            for i in range(self.m):
                if abs(resultDirect['w'][t,i])>epsLam:
                    listZeta.append(-resultDirect['lamG'][t,i]/resultDirect['w'][t,i])
                if abs(resultDirect['v'][t,i])>epsLam:
                    listZeta.append(-resultDirect['lamH'][t,i]/resultDirect['v'][t,i])

        zeta=max((0,max(listZeta)))+1
        resultDirect['beta']=resultDirect['lamH']+zeta*resultDirect['v']
        
        dynStateMatdm,dynMultMatdm,compStateMatdm,compMultMatdm,invU,self.matg=self.buildMatricesIndirectSystem(zeta)
        
        (initGuessTotal, initGuessShooting)=self.buildFirstGuess(resultDirect)
        
        solInteg=self.integDVIShooting(initGuessShooting, initGuessTotal,\
                          dynStateMatdm,dynMultMatdm,compStateMatdm,compMultMatdm,\
                          M,R,b,\
                          theta,print_level_ipopt)
        
        xNumDual=solInteg['x']
        pNumDual=solInteg['p']
        vNumDual=solInteg['v']
        betaNumDual=solInteg['beta']
        gapNewton=solInteg['gap']
        sensitJacob=solInteg['jacob']
        initGuessTotal={"x":xNumDual,"p":pNumDual,"v":vNumDual,"beta":betaNumDual}

        normEcNewt=max(abs(gapNewton.full()))[0]
        listEc=[]
        listEc.append(normEcNewt)

        nbLoops=0

        while ((nbLoops<maxLoops) and (normEcNewt>self.hIndirect)):
            if verbose:
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                print("Newton loops done so far: "+str(nbLoops))
                print("Norm inf on gap at shooting points: "+str(normEcNewt))
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
           

            initGuessPrev=[]
            for i in range(self.nbIntervShooting+1):
                initGuessPrev.append(vertcat(initGuessShooting['x'][i,:]).T)
                initGuessPrev.append(vertcat(initGuessShooting['p'][i,:]).T)
            
            initGuessShooting=self.newtonSolveShooting(sensitJacob,gapNewton,initGuessPrev)

            solInteg=self.integDVIShooting(initGuessShooting, initGuessTotal,\
                              dynStateMatdm,dynMultMatdm,compStateMatdm,compMultMatdm,\
                              M,R,b,
                              theta,print_level_ipopt)
            xNumDual=solInteg['x']
            pNumDual=solInteg['p']
            vNumDual=solInteg['v']
            betaNumDual=solInteg['beta']
            gapNewton=solInteg['gap']
            sensitJacob=solInteg['jacob']
            initGuessTotal={"x":xNumDual,"p":pNumDual,"v":vNumDual,"beta":betaNumDual}

            normEcNewt=max(abs(gapNewton.full()))[0]
            listEc.append(normEcNewt)
            nbLoops+=1
            
        if verbose:
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            print("Done!")
            print("Total loops : "+str(nbLoops))
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            
        uNumDual=DM.zeros(self.nbStepIndirect+1,self.m)
        wNumDual=DM.zeros(self.nbStepIndirect+1,self.m)
        lamH=DM.zeros(self.nbStepIndirect+1,self.m)
        lamG=DM.zeros(self.nbStepIndirect+1,self.m)
        for i in range(self.nbStepIndirect+1):
            uNumDual[i,:]=DM(mtimes(invU,mtimes(transpose(self.F),pNumDual[i,:].T)+\
                                   mtimes(transpose(self.E),betaNumDual[i,:].T-(zeta+self.r)*vNumDual[i,:].T)))
            wNumDual[i,:]=DM(mtimes(self.C,xNumDual[i,:].T)+mtimes(self.D,vNumDual[i,:].T)+mtimes(self.E,uNumDual[i,:].T))
            lamH[i,:]=DM(betaNumDual[i,:]-(zeta+self.r)*vNumDual[i,:])
            lamG[i,:]=-DM(mtimes(transpose(self.B),pNumDual[i,:].T)+mtimes(transpose(self.D),lamH[i,:].T))
        
        
        resultReturned={'x':xNumDual, 'u':uNumDual, 'v':vNumDual, 'w':wNumDual,\
                        'p':pNumDual,'lamG':lamG,'lamH':lamH, 'listGap':listEc,
                       'zeta':zeta}
        
        return resultReturned
    
    """
        Plot the results given by compute_optimal

        :param result: result given by compute_optimal.
        :type result: dict

        :param dual: if True, plot the associated multipliers of the trajectory (assuming the optimal solution is S-stationary).
        :type dual: boolean
        
        :param font: Parameters 'font' for matplotlib.rc
        :type resol: dict
        
        :param styleLines: Different style to be used to plot the lines.
        :type styleLines: array of strings
        
         :param save: if True, save the plot in directory given by saveFolder
        :type save: boolean
        
        :param saveFolder: path to directory where plots are saved
        :type save: string
        
        .. seealso:: compute_optimal
    """
    def resultVis(self,result, dual=False,
                 resol=150, font=None, styleLines=None,
                 save=False, saveFolder=os.path.join(os.path.curdir,"img")):
        if font:
            matplotlib.rc('font', **font)
            
        matplotlib.rcParams['lines.linewidth']=2
        if save:
            if not os.path.exists("saveFolder"):
                os.makedirs("saveFolder")
        
        N=self.nbStepIndirect
        tgrid = [self.t0+self.hIndirect*k for k in range(N+1)]
        
        if not styleLines:
            styleLines=['-', '--', '-.', ':']
        nbMaxStyle=int(len(styleLines))
        
            # State x
        plt.figure("State x", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.n):
            plt.plot(tgrid, result['x'][:,k], styleLines[k%nbMaxStyle])
            legend.append('x'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotx.eps'),format='eps')

            # Control u
        plt.figure("Control u", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.m):
            plt.plot(tgrid, result['u'][:,k], styleLines[k%nbMaxStyle])
            legend.append('u'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotu.eps'),format='eps')

            # Control v
        plt.figure("Control v", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.m):
            plt.plot(tgrid, result['v'][:,k], styleLines[k%nbMaxStyle])
            legend.append('v'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotv.eps'),format='eps')

            # Complementarity w=Cx+Dv+Eu
        plt.figure("Comp. w", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.m):
            plt.plot(tgrid, result['w'][:,k], styleLines[k%nbMaxStyle])
            legend.append('w'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotw.eps'),format='eps')
            
        nbIter=len(result['listGap'])
        if nbIter>1:
            plt.figure("List of gaps", dpi=resol)
            plt.clf()
            plt.scatter(range(nbIter),result['listGap'])
            plt.plot(range(nbIter),result['listGap'],'-.')
            plt.plot(range(nbIter),np.zeros(nbIter),'--',color='red')
            plt.xlabel('nb of iterations')
            plt.legend(['Max. gap at shooting nodes'],loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotGaps.eps'),format='eps')
            
        if dual:
            plt.figure("Adjoint p", dpi=resol)
            plt.clf()
            legend=[]
            for k in range(self.n):
                plt.plot(tgrid, result['p'][:,k], styleLines[k%nbMaxStyle])
                legend.append('p'+str(k+1))
            plt.xlabel('t')
            plt.legend(legend,loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotp.eps'),format='eps')
            
            plt.figure("Mult lamH", dpi=resol)
            plt.clf()
            legend=[]
            for k in range(self.m):
                plt.plot(tgrid, result['lamH'][:,k], styleLines[k%nbMaxStyle])
                legend.append('lamH'+str(k+1))
            plt.xlabel('t')
            plt.legend(legend,loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotlamH.eps'),format='eps')
                
            plt.figure("Mult lamG", dpi=resol)
            plt.clf()
            legend=[]
            for k in range(self.m):
                plt.plot(tgrid, result['lamG'][:,k], styleLines[k%nbMaxStyle])
                legend.append('lamG'+str(k+1))
            plt.xlabel('t')
            plt.legend(legend,loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotlamG.eps'),format='eps')
                
        if not save:
            plt.show()
        