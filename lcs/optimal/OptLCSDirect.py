from casadi import DM, SX, nlpsol, vertcat, mtimes, inv, transpose, solve, Function, if_else, reshape
from numpy import inf, floor
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os

from lcs.LCS import LCS
"""
    General class for optimal control problem of the type:
    .. math::
   :nowrap:

       \begin{eqnarray}
          \int_{t0}^{tf} (x(t)^\intercal Qx(t)+u(t)^\intercal Uu(t)) dt
          \dot{x}=Ax+Bv+Fu
          0\leq v \perp Cx+Dv+Fu \geq 0
          Mx(0) + Nx(T)= b
       \end{eqnarray}

    using direct method.

    :param x0: state at time t0 if fixed
    :type x0: np.ndarray

    :param xf: state at time tf if fixed
    :type xf: np.ndarray
    
    .. seealso:: 
"""

class OptLCSDirect(LCS):
   
    def __init__(self, A, B, C, D, E, F, Q, U, t0, tf):    
        LCS.__init__(self, A, B, C, D, E, F)
        self.Q=SX(Q)
        self.U=U
        self.t0=t0
        self.tf=tf
        
        self.initConstrained=False
        self.finalConstrained=False
        self.mixedConstrained=False
        
        self._x0=None
        self._xf=None
        self.matCondInit=None
        self.matCondFinal=None
        self.valCondMixed=None
        
    def _setnbStepDirect(self,N):
        self._nbStepDirect=N
        self.hDirect=(self.tf-self.t0)/float(N)
        
    def _getnbStepDirect(self):
        return self._nbStepDirect
        
    nbStepDirect = property(_getnbStepDirect,_setnbStepDirect)
    
    def _setInitialCond(self,x0):
        if self.mixedConstrained:
            raise ValueError('System already admits mixed constraints !')
        else:
            self._x0=x0
            self.initConstrained=True
        
    def _getInitialCond(self):
        if self.initConstrained:
            return self._x0
        else:
            return None
        
    def _delInitialCond(self):
        del self._x0
        self.initConstrained=False
        
    x0 = property(_getInitialCond, _setInitialCond, _delInitialCond)
    
    def _setFinalCond(self,xf):
        if self.mixedConstrained:
            raise ValueError('System already admits mixed constraints !')
        else:
            self._xf=xf
            self.finalConstrained=True
    
    def _getFinalCond(self):
        if self.initConstrained:
            return self._xf
        else:
            return None
    
    def _delFinalCond(self):
        del self._xf
        self.finalConstrained=False
        
    xf = property(_getFinalCond, _setFinalCond, _delFinalCond)
    
    def setMixedConstraints(self,M,N,b):
        if self.initConstrained or self.finalConstrained:
            raise ValueError('System already admits initial or final constraints !')
        else:
            self.matCondInit=M
            self.matCondFinal=N
            self.valCondMixed=b
            self.mixedConstrained=True
        
    def primalDeclar_augment(self):
        N=self.nbStepDirect

        x=SX.sym('x',self.n)
        u=SX.sym('u',self.m)
        v=SX.sym('v',self.m)

        #Evolution law
        dotx=mtimes(self.A,x)+mtimes(self.B,v)+mtimes(self.F,u)
        fx=Function('dotx',[x,u,v],[dotx])

        #Complementarity
        fw=mtimes(self.C,x)+mtimes(self.D,v)+mtimes(self.E,u)
        w=Function('w',[x,u,v],[fw])

        comp=Function('comp',[x,u,v],[mtimes(transpose(v),w(x,u,v))])

        #Cost
        pi=SX.sym('pi',1)
        fcout=mtimes(transpose(x),mtimes(self.Q,x))+mtimes(transpose(u),mtimes(self.U,u))+pi*comp(x,u,v)
        cout=Function('cout',[x,u,v,pi],[fcout])

        dictLaws={'comp':comp, 'dotx':fx, 'cost':cout, 'w':w}

        #Parameters
        h = (self.tf-self.t0)/float(N)

        #Discretized variables
        lxb=[]
        uxb=[]

        xk=[]
        #Initial Condition
        xk.append(SX.sym('xk_0',self.n))
        if self.initConstrained:
            lxb.append(self.x0)
            uxb.append(self.x0)
        else:
            lxb.append(-inf*np.ones(self.n))
            uxb.append(inf*np.ones(self.n))
        #Middle Points
        for i in range(1,N-1):
            xk.append(SX.sym('xk_'+str(i),self.n))
            lxb.append(-inf*np.ones(self.n))
            uxb.append(inf*np.ones(self.n))
        #Final Condition
        xk.append(SX.sym('xk_'+str(N),self.n))
        if self.finalConstrained:
            lxb.append(self.xf)
            uxb.append(self.xf)
        else:
            lxb.append(-inf*np.ones(self.n))
            uxb.append(inf*np.ones(self.n))

        uk=[]
        for i in range(N-1):
            uk.append(SX.sym('uk_'+str(i),self.m))
            lxb.append(-inf*np.ones(self.m))
            uxb.append(inf*np.ones(self.m))

        vk=[]
        for i in range(N-1):
            vk.append(SX.sym('vk_'+str(i),self.m))
            lxb.append(np.zeros(self.m))
            uxb.append(inf*np.ones(self.m))

        vark=xk+uk+vk

        #Discretized cost
        obj=SX(0)
        for i in range(N-1):
            obj+=cout(xk[i],uk[i],vk[i],pi)
        obj+=cout(xk[N-1],SX.zeros(self.m),SX.zeros(self.m),pi)

        #Discretized constraints (dynamics,complementarity)
        g=[]
        lgb=[]
        ugb=[]
        for i in range(1,N):
            g.append((xk[i]-xk[i-1])/self.hDirect-fx(xk[i],uk[i-1],vk[i-1]))
            lgb.append(np.zeros(self.n))
            ugb.append(np.zeros(self.n))


        for i in range(N-1):
            g.append(w(xk[i],uk[i],vk[i]))
            lgb.append(np.zeros(self.m))
            ugb.append(inf*np.ones(self.m))
    
        if self.mixedConstrained:
            g.append(mtimes(self.matCondInit,xk[0])+mtimes(self.matCondFinal,xk[N-1])-self.valCondMixed)
            ugb.append(np.zeros(self.n))
            lgb.append(np.zeros(self.n))

        return {'obj':obj, 'var':vark, 'lbx':lxb, 'ubx':uxb, 'g':g, 'lbg':lgb, 'ubg':ugb, 'pi':pi, "laws":dictLaws}

        
    def primalDeclar_relax(self):
        N=self.nbStepDirect
        
        x=SX.sym('x',self.n)
        u=SX.sym('u',self.m)
        v=SX.sym('v',self.m)

        #Evolution law
        dotx=mtimes(self.A,x)+mtimes(self.B,v)+mtimes(self.F,u)
        fx=Function('dotx',[x,u,v],[dotx])

        #Cost
        fcout=mtimes(transpose(x),mtimes(self.Q,x))+mtimes(transpose(u),mtimes(self.U,u))
        cout=Function('cout',[x,u],[fcout])

        #Complementarity
        fw=mtimes(self.C,x)+mtimes(self.D,v)+mtimes(self.E,u)
        w=Function('w',[x,u,v],[fw])

        t=SX.sym('t',1)
        fcomp=SX.sym('fcomp',self.m)
        for i in range(self.m):
            fcomp[i]=if_else(w(x,u,v)[i]+v[i]>=2*t,(w(x,u,v)[i]-t)*(v[i]-t),-0.5*((w(x,u,v)[i]-t)**2+(v[i]-t)**2))

        phi=Function('phi',[x,u,v,t],[fcomp])

        dictLaws={'comp':phi, 'dotx':fx, 'cost':cout, 'w':w}
        
        #Discretized variables
        lxb=[]
        uxb=[]

        xk=[]
        #Initial Condition
        xk.append(SX.sym('xk_0',self.n))
        if self.initConstrained:
            lxb.append(self.x0)
            uxb.append(self.x0)
        else:
            lxb.append(-inf*np.ones(self.n))
            uxb.append(inf*np.ones(self.n))
        #Middle points
        for i in range(1,N-1):
            xk.append(SX.sym('xk_'+str(i),self.n))
            lxb.append(-inf*np.ones(self.n))
            uxb.append(inf*np.ones(self.n))
        #Terminal Condition
        xk.append(SX.sym('xk_'+str(N-1),self.n))
        if self.finalConstrained:
            lxb.append(self.xf)
            uxb.append(self.xf)
        else:
            lxb.append(-inf*np.ones(self.n))
            uxb.append(inf*np.ones(self.n))

        uk=[]
        for i in range(N-1):
            uk.append(SX.sym('uk_'+str(i),self.m))
            lxb.append(-inf*np.ones(self.m))
            uxb.append(inf*np.ones(self.m))

        vk=[]
        for i in range(N-1):
            vk.append(SX.sym('vk_'+str(i),self.m))
            lxb.append(np.zeros(self.m))
            uxb.append(inf*np.ones(self.m))

        vark=xk+uk+vk

        #Discretized cost
        obj=SX(0)
        for i in range(N-1):
        #for i in range(N):
            obj+=self.hDirect*cout(xk[i],uk[i])
        obj+=self.hDirect*cout(xk[N-1],SX.zeros(self.m))

        #Discretized constraints (dynamics,complementarity)
        g=[]
        lgb=[]
        ugb=[]
        for i in range(1,N):
            g.append((xk[i]-xk[i-1])/self.hDirect-fx(xk[i],uk[i-1],vk[i-1]))
            lgb.append(np.zeros(self.n))
            ugb.append(np.zeros(self.n))


        for i in range(N-1):
            g.append(w(xk[i],uk[i],vk[i]))
            lgb.append(np.zeros(self.m))
            ugb.append(inf*np.ones(self.m))

        for i in range(N-1):
            g.append(phi(xk[i],uk[i],vk[i],t))
            ugb.append(np.zeros(self.m))
            lgb.append(-inf*np.ones(self.m))

        if self.mixedConstrained:
            g.append(mtimes(self.matCondInit,xk[0])+mtimes(self.matCondFinal,xk[N-1])-self.valCondMixed)
            ugb.append(np.zeros(self.n))
            lgb.append(np.zeros(self.n))
            
        return {'obj':obj, 'var':vark, 'lbx':lxb, 'ubx':uxb, 'g':g, 'lbg':lgb, 'ubg':ugb, 't':t, "laws":dictLaws}
    
    def compVio(g,h):
        n=g.size()[0]
        result=min(g[0],h[0])
        for i in range(1,n):
            result=max(result,min(g[i],h[i]))
        return result
    
    def optimLoop_augment(self,\
                        print_level_ipopt=0, verbose=0,\
                        maxIter=100,eps=1e-12,gamma=0.4,delta=0.9):
        dictDeclar=self.primalDeclar_augment()
        N=self.nbStepDirect
        w=dictDeclar['laws']["w"]

        #Optimisation solver
        nlp={'x':vertcat(*(dictDeclar['var'])),'p':dictDeclar['pi'],'f':dictDeclar['obj'],'g':vertcat(*(dictDeclar['g']))}

        mu=1
        epsComp=1
        epspen=1
        opts = {}
        opts["ipopt"] = {'print_level':print_level_ipopt,
                             "nlp_scaling_method":"none",
                             "constr_viol_tol":epspen,
                             "tol":epspen,
                             "mu_strategy":"adaptive",
                             "mu_target":mu,
                             "mu_init":mu,
                            'fixed_variable_treatment':'make_constraint'}
        solver=nlpsol('solver','ipopt',nlp,opts)

        it=0
        pii=1
        
        vNum0=vertcat(*np.zeros((self.m*(N-1),1)))
        uNum0=vertcat(*(0*np.ones((self.m*(N-1),1))))

        if self.initConstrained:
            xNum0=self.x0
        else:
            xNum0=np.zeros((self.n,1))
        xNum0=np.append(xNum0,np.zeros((self.n*(N-2),1)))
        if self.finalConstrained:
            xNum0=np.append(xNum0,self.xf)
        else:
            xNum0=np.append(xNum0,np.zeros((self.n,1)))
        xNum0=DM(vertcat(*xNum0))

        wNum0=[]
        for i in range(N-1):
            wNum0.append(w(xNum0[self.n*i:self.n*(i+1)],\
                          uNum0[self.m*i:(i+1)*self.m],\
                          vNum0[self.m*i:(i+1)*self.m]))

        wNum0=vertcat(*wNum0)

        #Optimisation loop
        goto2=True

        while ((it<maxIter)&(epspen>eps))|(it==0):
            #First step
            if not goto2:
                mu=mu**(1+delta)
                epspen/=10
                epsComp=mu**(gamma)
                opts["ipopt"] = {'print_level':print_level_ipopt,
                             "nlp_scaling_method":"none",
                             "constr_viol_tol":epspen,
                             "tol":epspen,
                             "mu_strategy":"adaptive",
                             "mu_target":mu,
                             "mu_init":mu,}
                solver=nlpsol('solver','ipopt',nlp,opts)

            #Second step
            v0=vertcat(*[xNum0,uNum0,vNum0])
            sol = solver(x0=v0,
                         p=pii,
                         lbx=vertcat(*(dictDeclar['lbx'])), 
                         ubx=vertcat(*(dictDeclar['ubx'])),
                         lbg=vertcat(*(dictDeclar['lbg'])), 
                         ubg=vertcat(*(dictDeclar['ubg'])))
            solNum=sol['x']
            xNum0=solNum[0:self.n*N]
            uNum0=solNum[self.n*N:N*self.n+(N-1)*self.m]
            vNum0=solNum[N*self.n+(N-1)*self.m:N*self.n+(N-1)*2*self.m]
            wNum0=[]
            for i in range(N-1):
                wNum0.append(w(xNum0[self.n*i:self.n*(i+1)],\
                               uNum0[self.m*i:(i+1)*self.m],\
                               vNum0[self.m*i:(i+1)*self.m]))
            wNum0=vertcat(*wNum0)

            #Third step
            if bool(OptLCSDirect.compVio(vNum0,wNum0)<epsComp):
                goto2=False
                it+=1
                if verbose:
                    print('**********************************************')
                    print('Number of IPOPT calls so far : ', it)
                    print('**********************************************')
            else:
                pii*=10
                goto2=True
                if verbose:
                    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                    print('Increasing pi : ',pii)
                    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')


        if verbose:
            print('**********************************************')
            print("Total number of IPOPT calls: ", it)
            print('**********************************************')

            
        xNum=DM(reshape(vertcat(xNum0),self.n, N).T)
        uNum=DM(reshape(vertcat(uNum0),self.m,N-1).T)
        vNum=DM(reshape(vertcat(vNum0),self.m,N-1).T)
        wNum=DM(reshape(vertcat(wNum0),self.m,N-1).T)
        
        resultPrim={'x':xNum, 'v':vNum, 'u':uNum, 'w':wNum}
        multOptim={'lam_x':sol['lam_x'], 'lam_g':sol['lam_g']}
        
        return (resultPrim, multOptim)
    
    
    def optimLoop_relax(self,\
                         print_level_ipopt=0, verbose=0,\
                         maxIter=100,eps=1e-15,tmin=1e-15,sigma=1e-2):
        dictDeclar=self.primalDeclar_relax()
        m=self.m
        n=self.n
        N=self.nbStepDirect
        
        w=dictDeclar['laws']["w"]
        
        nlp={'x':vertcat(*(dictDeclar['var'])),'p':dictDeclar['t'],'f':dictDeclar['obj'],'g':vertcat(*(dictDeclar['g']))}
        solver=nlpsol('solver','ipopt',nlp,
                        {'ipopt':{'print_level':print_level_ipopt, 'fixed_variable_treatment':'make_constraint',
                        "nlp_scaling_method":"none", "start_with_resto":"yes"}})

        it=0
        tk=0.1/sigma

        lxb=dictDeclar['lbx']
        uxb=dictDeclar['ubx']
        lgb=dictDeclar['lbg']
        ugb=dictDeclar['ubg']

        vNum0=vertcat(*np.zeros((self.m*(N-1),1)))
        uNum0=vertcat(*(0*np.ones((self.m*(N-1),1))))

        if self.initConstrained:
            xNum0=self.x0
        else:
            xNum0=np.zeros((n,1))
        xNum0=np.append(xNum0,np.zeros((n*(N-2),1)))
        if self.finalConstrained:
            xNum0=np.append(xNum0,self.xf)
        else:
            xNum0=np.append(xNum0,np.zeros((n,1)))
        xNum0=DM(vertcat(*xNum0))

        wNum0=[]
        for i in range(N-1):
            wNum0.append(w(xNum0[n*i:n*(i+1)],uNum0[m*i:(i+1)*m],vNum0[m*i:(i+1)*m]))

        wNum0=vertcat(*wNum0)

        checkComp=OptLCSDirect.compVio(vNum0,wNum0)

        #Optimisation loop
        while ((it<maxIter)&bool(tmin<=tk)&bool(checkComp>eps))|(it==0):
            v0=vertcat(*[xNum0,uNum0,vNum0])
            sol = solver(x0=v0,
                         p=tk,
                         lbx=vertcat(*lxb), 
                         ubx=vertcat(*uxb),
                         lbg=vertcat(*lgb), 
                         ubg=vertcat(*ugb))
            solNum=sol['x']
            xNum0=solNum[0:n*N]
            uNum0=solNum[n*N:N*n+(N-1)*m]
            vNum0=solNum[N*n+(N-1)*m:N*n+(N-1)*2*m]
            wNum0=[]
            for i in range(N-1):
                wNum0.append(w(xNum0[n*i:n*(i+1)],uNum0[m*i:(i+1)*m],vNum0[m*i:(i+1)*m]))
            wNum0=vertcat(*wNum0)
            checkComp=OptLCSDirect.compVio(vNum0,wNum0)
            tk=sigma*min(tk,checkComp)
            it+=1

            if verbose:
                print('**********************************************')
                print('Number of IPOPT calls so far : ', it)
                print('Complementarity violation : ', checkComp)
                print('**********************************************')

        v0=vertcat(*[xNum0,uNum0,vNum0])
        sol = solver(x0=v0,p=tk,lbx=vertcat(*lxb), ubx=vertcat(*uxb),lbg=vertcat(*lgb), ubg=vertcat(*ugb))
        solNum=sol['x']
        xNum0=solNum[0:n*N]
        uNum0=solNum[n*N:N*n+(N-1)*m]
        vNum0=solNum[N*n+(N-1)*m:N*n+(N-1)*2*m]
        wNum0=[]
        for i in range(N-1):
            wNum0.append(w(xNum0[n*i:n*(i+1)],uNum0[m*i:(i+1)*m],vNum0[m*i:(i+1)*m]))
        wNum0=vertcat(*wNum0)
        it+=1
        
        if verbose:
            print('**********************************************')
            print("Total number of IPOPT calls : ", it)
            print('**********************************************')

        xNum=DM(reshape(vertcat(xNum0),self.n, N).T)
        uNum=DM(reshape(vertcat(uNum0),self.m,N-1).T)
        vNum=DM(reshape(vertcat(vNum0),self.m,N-1).T)
        wNum=DM(reshape(vertcat(wNum0),self.m,N-1).T)

        resultPrim={'x':xNum, 'v':vNum, 'u':uNum, 'w':wNum}
        multOptim={'lam_x':sol['lam_x'], 'lam_g':sol['lam_g']}
        
        return (resultPrim, multOptim)
    
    
    def dualDeclar(self, result):
        N=self.nbStepDirect

        #Discretized variables
        lpb=[]
        upb=[]

        pk=[]
        #Initial Condition
        pk.append(SX.sym('pk_0',self.n))
        if self.initConstrained:
            lpb.append(-np.inf*np.ones(self.n))
            upb.append(np.inf*np.ones(self.n))
        else:
            lpb.append(np.zeros(self.n))
            upb.append(np.zeros(self.n))

        #Middle Points
        for i in range(1,N-1):
            pk.append(SX.sym('pk_'+str(i),self.n))
            lpb.append(-np.inf*np.ones(self.n))
            upb.append(np.inf*np.ones(self.n))
        #Terminal Condition
        pk.append(SX.sym('pk_'+str(N-1),self.n))
        if self.finalConstrained:
            lpb.append(-np.inf*np.ones(self.n))
            upb.append(np.inf*np.ones(self.n))
        else:
            lpb.append(0*np.ones(self.n))
            upb.append(0*np.ones(self.n))

        lamH=[]
        lamG=[]
        for i in range(N-1):
            lamH.append(SX.sym('lamH_'+str(i),self.m))
            lamG.append(SX.sym('lamG_'+str(i),self.m))
            lpb.append(-np.inf*np.ones(2*self.m))
            upb.append(np.inf*np.ones(2*self.m))
 
        varpk=vertcat(*pk,*lamH,*lamG)

        obj=[]
        gp=[]
        for i in range(N-1):
            obj.append(mtimes(self.Q,result['x'][i,:].T)\
                      -mtimes(self.A+SX.eye(self.n)/self.hDirect,pk[i+1])+pk[i]/self.hDirect\
                      -mtimes(transpose(self.C),lamH[i]))

        for i in range(N-1):
            obj.append(mtimes(self.U,result['u'][i,:].T)\
                     -mtimes(transpose(self.F),pk[i])\
                     -mtimes(transpose(self.E),lamH[i]))

        for i in range(N-1):
            obj.append(mtimes(transpose(self.B),pk[i])+lamG[i]\
                      +mtimes(transpose(self.D),lamH[i]))

        gp=[]
        lgp=[]
        ugp=[]
        for t in range(N-1):
            for i in range(self.m):
                if (result['v'][t,i]>1e-5 and abs(result['w'][t,i])<1e-5):
                    gp.append(lamG[t][i])
                    lgp.append(0)
                    ugp.append(0)
                elif (result['w'][t,i]>1e-5 and abs(result['v'][t,i])<1e-5):
                    gp.append(lamH[t][i])
                    lgp.append(0)
                    ugp.append(0)
                else:
                    gp.append(lamG[t][i])
                    gp.append(lamH[t][i])
                    lgp.append(np.zeros(2))
                    ugp.append(np.inf*np.ones(2))


        #Quadratic objective
        obj=vertcat(*obj)
        obj=mtimes(transpose(obj),obj)

        return {'var':varpk, 'lpb':lpb, 'upb':upb, 'lgp':lgp, 'ugp':ugp, 'obj':obj, 'g':gp}
        
        
    def dualSolv(self,result, lam_x, lam_g, print_level_ipopt=0):
        N=self.nbStepDirect

        declar=self.dualDeclar(result)

        nlpMult={'x':declar['var'],'f':declar['obj'], 'g':vertcat(*declar['g'])}
        optSolv={}
        optSolv['ipopt']={'print_level':print_level_ipopt,\
                          'fixed_variable_treatment':'make_constraint',\
                          "nlp_scaling_method":"none"}
        solverMult=nlpsol('solver','ipopt',nlpMult,optSolv)

        mulCtrt={}
        mulCtrt['fx']=reshape(lam_g[0:self.n*(N-1)],N-1,self.n)
        mulCtrt['w']=reshape(lam_g[self.n*(N-1):(self.n+self.m)*(N-1)],N-1,self.m)

        mulv=reshape(lam_x[N*self.n+(N-1)*self.m:N*self.n+(N-1)*2*self.m],N-1,self.m)

        v0=[]
        for i in range(N-1):
            v0.append(mulCtrt['fx'][i,:].T)
        v0.append(DM.zeros(self.n))

        for i in range(N-1):
            v0.append(mulv[i,:].T)

        for i in range(N-1):
            v0.append(mulCtrt['w'][i,:].T)

        v0=vertcat(*v0)

        #Solving Process
        solMult=solverMult(x0=v0,
                            lbx=vertcat(*(declar['lpb'])),
                            ubx=vertcat(*(declar['upb'])),
                            lbg=vertcat(*(declar['lgp'])), 
                            ubg=vertcat(*(declar['ugp']))
                           )
        #Retrieve results
        solNumMult=solMult['x']
        pNum=solNumMult[0:self.n*N]

        lamHNum=solNumMult[self.n*N:self.n*N+(N-1)*self.m]
        lamGNum=solNumMult[N*self.n+(N-1)*self.m:N*self.n+(N-1)*2*self.m]
        
        return {'p':DM(reshape(vertcat(pNum),self.n,N).T),\
                'lamG':DM(reshape(vertcat(lamGNum),self.m,N-1).T),\
                'lamH':DM(reshape(vertcat(lamHNum),self.m,N-1).T)}

        
        
    """
        Compute an approximate optimal solution using direct method. In order to deal with the complementarity, 2 methods are available, taken for the litterature : augmentation of the cost, or relaxation of the complementarity.

        :param method: define what method should be used for computing the optimal solution. "relax" for relaxation of the complementarity, "augment" for augmentation of the cost.
        :type method: string

        :param dual: if True, compute the associated multipliers of the trajectory (assuming the optimal solution is S-stationary).
        :type dual: boolean
        
        :param print_level_ipopt: Level of information given by ipopt 
        :type print_level_ipopt: integer between 0 and 9
        
        :param verbose: Control verbosity
        :type verbose: boolean
        
        :return: dict containing:
            - optimal "x", "u" and "v", and associated "w"
        and if dual:    
            - adjoint state "p"
            - multipliers "lamG" and "lamH"
        
        .. seealso:: 
    """
    def compute_optimal(self,method='relax', dual=False, print_level_ipopt=0, verbose=0):
        if method=='relax':
            (resultPrim, multOptim)=self.optimLoop_relax(print_level_ipopt=print_level_ipopt, verbose=verbose)
        elif method=='augment':
            (resultPrim, multOptim)=self.optimLoop_augment(print_level_ipopt=print_level_ipopt, verbose=verbose)
        else:
            raise ValueError('No method associated to that name')
        
        if dual:
            resultDual=self.dualSolv(resultPrim, multOptim['lam_x'], multOptim['lam_g'], print_level_ipopt=print_level_ipopt)
            result={'x':resultPrim['x'], 'u':resultPrim['u'], 'v':resultPrim['v'], 'w':resultPrim['w'],\
                'p':resultDual['p'],'lamG':resultDual['lamG'],'lamH':resultDual['lamH']}
        else:
            result={'x':resultPrim['x'], 'u':resultPrim['u'], 'v':resultPrim['v'], 'w':resultPrim['w']}
        
        return result
    
        """
        Plot the results given by compute_optimal

        :param result: result given by compute_optimal.
        :type result: dict

        :param dual: if True, plot the associated multipliers of the trajectory (assuming the optimal solution is S-stationary).
        :type dual: boolean
        
        :param font: Parameters 'font' for matplotlib.rc
        :type resol: dict
        
        :param styleLines: Different style to be used to plot the lines.
        :type styleLines: array of strings
        
         :param save: if True, save the plot in directory given by saveFolder
        :type save: boolean
        
        :param saveFolder: path to directory where plots are saved
        :type save: string
        
        .. seealso:: compute_optimal
    """
    def resultVis(self,result, dual=False,
                 resol=150, font=None, styleLines=None,
                 save=False, saveFolder=os.path.join(os.path.curdir,"img")):

        if font:
            matplotlib.rc('font', **font)
        matplotlib.rcParams['lines.linewidth']=2 
            
        if save:
            if not os.path.exists(saveFolder):
                os.makedirs(saveFolder)
        
        N=self.nbStepDirect
        tgrid = [self.t0+self.hDirect*k for k in range(N)]
        
        if not styleLines:
            styleLines=['-', '-.', '--', ':']
        nbMaxStyle=int(len(styleLines))
       
            # State x
        plt.figure("State x", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.n):
            plt.plot(tgrid, result['x'][:,k], styleLines[k%nbMaxStyle])
            legend.append('x'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotx.eps'),format='eps')

            # Control u
        plt.figure("Control u", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.m):
            plt.plot(tgrid, vertcat(result['u'][:,k],DM.nan(1)), styleLines[k%nbMaxStyle])
            legend.append('u'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotu.eps'),format='eps')


            # Control v
        plt.figure("Control v", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.m):
            plt.plot(tgrid, vertcat(result['v'][:,k],DM.nan(1)), styleLines[k%nbMaxStyle])
            legend.append('v'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotv.eps'),format='eps')

            # Complementarity w=Cx+Dv+Eu
        plt.figure("Comp. w", dpi=resol)
        plt.clf()
        legend=[]
        for k in range(self.m):
            plt.plot(tgrid, vertcat(result['w'][:,k],DM.nan(1)), styleLines[k%nbMaxStyle])
            legend.append('w'+str(k+1))
        plt.xlabel('t')
        plt.legend(legend,loc=0)
        plt.grid()
        if save:
            plt.savefig(os.path.join(saveFolder,'plotw.eps'),format='eps')
            
        if dual:
            plt.figure("Adjoint p", dpi=resol)
            plt.clf()
            legend=[]
            for k in range(self.n):
                plt.plot(tgrid, result['p'][:,k], styleLines[k%nbMaxStyle])
                legend.append('p'+str(k+1))
            plt.xlabel('t')
            plt.legend(legend,loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotp.eps'),format='eps')
                
            plt.figure("Mult lamH", dpi=resol)
            plt.clf()
            legend=[]
            for k in range(self.m):
                plt.plot(tgrid, vertcat(result['lamH'][:,k],DM.nan(1)), styleLines[k%nbMaxStyle])
                legend.append('lamH'+str(k+1))
            plt.xlabel('t')
            plt.legend(legend,loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotlamH.eps'),format='eps')
            
            plt.figure("Mult lamG", dpi=resol)
            plt.clf()
            legend=[]
            for k in range(self.m):
                plt.plot(tgrid, vertcat(result['lamG'][:,k],DM.nan(1)), styleLines[k%nbMaxStyle])
                legend.append('lamG'+str(k+1))
            plt.xlabel('t')
            plt.legend(legend,loc=0)
            plt.grid()
            if save:
                plt.savefig(os.path.join(saveFolder,'plotlamG.eps'),format='eps')
        
        if not save:
            plt.show()
